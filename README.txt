ZURB Responsive Embed

This module adds a Link Field Formatter for Drupal 8 built upon Zurb Foundation 6 Responsive Embed component.

Included formatters
---
By default this modules includes three formatters:

- Zurb Responsive Embed (4:3)
- Zurb Responsive Embed Panorama (256:81)
- Zurb Responsive Embed Widescreen (16:9)

Installation
---
First of all you have to download the module and place it inside your *module* folder inside your Drupal project. You can do this with Composer:

  composer require drupal/zurb_video_field


How it works
---
- Add a new field to your content type
- Choose Link from the Field Type dropdown
- Customize the settings of this field as you want
- Under the Content Type display tab choose one of the Zurb Video formatter
- Have fun!
